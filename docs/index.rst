.. _swh-loader-pypi:

Software Heritage - PyPI loader
===============================

Loader for `PyPI <https://pypi.org/>`_ source code releases.


Reference Documentation
-----------------------

.. toctree::
   :maxdepth: 2

   /apidoc/swh.loader.pypi
